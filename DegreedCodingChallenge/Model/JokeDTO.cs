﻿using Newtonsoft.Json;

namespace DegreedCodingChallenge.Model
{
    public class JokeDTO
    {

        [JsonProperty("joke")]
        public string Text { get; set; }
    }
}
