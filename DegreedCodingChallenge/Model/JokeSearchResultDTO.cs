﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DegreedCodingChallenge.Model
{
    public class JokeSearchResultDTO
    {

        public List<JokeDTO> ShortJokes { get; set; }

        public List<JokeDTO> MediumJokes { get; set; }

        public List<JokeDTO> LongJokes { get; set; }

        public int TotalJokes { get; set; }

    }
}
