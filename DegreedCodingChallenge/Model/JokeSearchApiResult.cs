﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DegreedCodingChallenge.Model
{
    public class JokeSearchApiResult
    {
        [JsonProperty("results")]
        public List<JokeDTO> Jokes { get; set; }

        [JsonProperty("total_jokes")]
        public int TotalJokes { get; set; }
    }
}
