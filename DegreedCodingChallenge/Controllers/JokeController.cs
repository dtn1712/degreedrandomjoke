﻿using System.Collections.Generic;
using DegreedCodingChallenge.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace DegreedCodingChallenge.Controllers
{

    public class ApiException : Exception
    {
        public int StatusCode { get; set; }

        public string Content { get; set; }
    }

    [ApiController]
    [Route("api/v1/[controller]")]
    public class JokeController : ControllerBase
    {

        private readonly ILogger<JokeController> _logger;

        public JokeController(ILogger<JokeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<JokeDTO> GetRandomJoke()
        {
            using (var client = new HttpClient())
            {
               
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(Constants.JokeBaseUrl);

                var stream = await response.Content.ReadAsStreamAsync();

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        return Helper.DeserializeJsonFromStream<JokeDTO>(stream);
                    }
                    catch (Exception e)
                    {
                        _logger.LogWarning(LoggingEvents.DeserializeRandomJoke, e, "Failed to deserialize joke");
                        throw new ApiException
                        {
                            StatusCode = (int)response.StatusCode,
                            Content = "An error occurred. Please try again"
                        };
                    }
                }

                var content = await Helper.StreamToStringAsync(stream);
                _logger.LogWarning(LoggingEvents.GetRandomJoke, "Failed to get random joke: {Content}", content);
                throw new ApiException
                {
                    StatusCode = (int)response.StatusCode,
                    Content = content
                };


            }
        }

        [HttpGet("search")]
        public async Task<JokeSearchResultDTO> Search(string query)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var url = String.Format(Constants.JokeBaseUrl + "search?term={0}&limit={1}", query, Constants.NumberJokeSearch);

                HttpResponseMessage response = await client.GetAsync(url);

                var stream = await response.Content.ReadAsStreamAsync();

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var apiResult = Helper.DeserializeJsonFromStream<JokeSearchApiResult>(stream);
                        return convertApiResultToDTO(apiResult);
                    }
                    catch (Exception e)
                    {
                        _logger.LogWarning(LoggingEvents.DeserializeSearchJoke, e, "Failed to deserialize search joke result");
                        throw new ApiException
                        {
                            StatusCode = (int)response.StatusCode,
                            Content = "An error occurred. Please try again"
                        };
                    }
                }

                var content = await Helper.StreamToStringAsync(stream);
                _logger.LogWarning(LoggingEvents.SearchJoke, "Failed to get random joke: {Content}", content);
                throw new ApiException
                {
                    StatusCode = (int)response.StatusCode,
                    Content = content
                };
            }
        }

        private JokeSearchResultDTO convertApiResultToDTO(JokeSearchApiResult apiResult)
        {
            var dto = new JokeSearchResultDTO
            {
                TotalJokes = apiResult.TotalJokes
            };
            if (dto.TotalJokes == 0) return dto;

            List<JokeDTO> shortJokes = new List<JokeDTO>();
            List<JokeDTO> mediumJokes = new List<JokeDTO>();
            List<JokeDTO> longJokes = new List<JokeDTO>();

            foreach (var joke in apiResult.Jokes)
            {
                string[] words = joke.Text.Split(' ');
                if (words.Length < 10)
                {
                    shortJokes.Add(joke);
                } else if (words.Length >= 10 && words.Length < 20)
                {
                    mediumJokes.Add(joke);
                } else
                {
                    longJokes.Add(joke);
                }
            }

            dto.ShortJokes = shortJokes;
            dto.MediumJokes = mediumJokes;
            dto.LongJokes = longJokes;

            return dto;
        }
    }
}
