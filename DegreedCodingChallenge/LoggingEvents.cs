﻿using System;
namespace DegreedCodingChallenge
{
    public class LoggingEvents
    {
        public const int SearchJoke = 1000;
        public const int GetRandomJoke = 1001;
        public const int DeserializeRandomJoke = 1002;
        public const int DeserializeSearchJoke = 1003;
    }
}
