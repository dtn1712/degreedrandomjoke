﻿import { get } from './resource';

export default class JokeResource {

    static getRandomJoke() {
        return get('api/v1/joke')({});
    }

    static searchJoke(query) {
      return get('api/v1/joke/search')({ query: query })
    }
}
