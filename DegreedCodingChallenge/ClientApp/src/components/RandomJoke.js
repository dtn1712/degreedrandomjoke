import React, { Component } from 'react';

import "./RandomJoke.css";

import JokeResource from '../resources/Joke';

const GET_JOKE_INTERVAL_SECONDS = 10;

export class RandomJoke extends Component {
  static displayName = RandomJoke.name;

  constructor(props) {
    super(props);
    this.state = {
      randomJoke: null,
      counter: GET_JOKE_INTERVAL_SECONDS
    };
  }

 getData() {
    JokeResource.getRandomJoke().then(res => {
      if (res && res.text) {
        this.setState({
          randomJoke: res.text,
          counter: GET_JOKE_INTERVAL_SECONDS
        })
      }
    });
  }

  componentDidMount() {
    this.getData();
    this.interval = setInterval(() => {
      this.countDown();
    }, 1000);
  }

  countDown() {
    let { counter } = this.state;
    counter = counter - 1;
    if (counter == 0) {
      this.getData();
    } else {
      this.setState({
        counter
      })
    }
  }

  componentWillUnmount() {
   clearInterval(this.interval);
  }

  render () {

    const { randomJoke, counter }  = this.state;

    return (
        <div>
          {randomJoke && (
            <div className='randomJoke'>
              <p>{randomJoke}</p>
              <p>(New joke in {counter} seconds) </p>
            </div>

          )}
        </div>
    );
  }
}
