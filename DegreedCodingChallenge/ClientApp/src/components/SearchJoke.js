import React, { Component } from 'react';
import parse from 'html-react-parser';

import { Input } from 'antd';

import "antd/dist/antd.css";

import "./SearchJoke.css";

import JokeResource from '../resources/Joke';

const NUMBER_JOKE_SEARCH = 30;

const { Search } = Input;

export class SearchJoke extends Component {
  static displayName = SearchJoke.name;

  constructor(props) {
    super(props);
    this.state = {
      shortJokes: [],
      mediumJokes: [],
      longJokes: [],
      totalJokes: 0,
      searchTerm: null,
      searchLoading: false
    };
  }

  onSearch(value) {
    this.setState({
      searchLoading: true
    })
    JokeResource.searchJoke(value).then(res => {
      if (res) {
        let { shortJokes, mediumJokes, longJokes, totalJokes } = res;
        this.setState({
          shortJokes,
          mediumJokes,
          longJokes,
          totalJokes,
          searchTerm: value,
          searchLoading: false
        });
      }
    })
  }

  htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
  }

  displaySearchJokes(searchTerm, jokes) {
    const listItems = jokes.map((joke) => {
      const jokeText = joke.text;
      let jokeWords = jokeText.split(" ");
      let htmlContent = "";
      for (var i = 0; i < jokeWords.length; i++) {
        let word = jokeWords[i];
        let filterWord = word.replace(/[^a-zA-Z0-9]/g, '');
        if (filterWord.toLowerCase() === searchTerm.toLowerCase()) {
          let index = word.toLowerCase().indexOf(searchTerm.toLowerCase());
          htmlContent = htmlContent + "<b>" + word.substring(index, index + searchTerm.length) + "</b>" + word.substring(index + searchTerm.length, word.length) + " ";
        } else {
          htmlContent = htmlContent + word + " ";
        }
      }
      return (
        <li>
          <span>{parse(htmlContent)}</span>
        </li>
      );
    });

    return (
      <ul>
        {listItems}
      </ul>
    );
  }

  render () {

    const { shortJokes, mediumJokes, longJokes, totalJokes, searchTerm, searchLoading }  = this.state;

    return (
        <div>
          <div className='searchJoke'>
            <div className="row">
              <div className="col-md-2"></div>
              <div className="col-md-8">
                <Search placeholder="Search Any Joke" onSearch={value => this.onSearch(value)} enterButton />
                {searchLoading && (
                  <div className="loading">
                    <p><em>Searching...</em></p>
                  </div>
                )}
                {!searchLoading && searchTerm && (
                  <div className="searchJokeResult">
                    {totalJokes > NUMBER_JOKE_SEARCH && (
                      <b>{totalJokes} result for "{searchTerm}" (Only first {NUMBER_JOKE_SEARCH} results will be shown )</b>
                    )}
                    {totalJokes <= NUMBER_JOKE_SEARCH && (
                      <b>{totalJokes} result for "{searchTerm}"</b>
                    )}
                  </div>
                )}

                {!searchLoading && shortJokes && shortJokes.length > 0 && (
                  <div className="shortJokes">
                    <b> Short Jokes - less than 10 words ({shortJokes.length})</b>
                    {this.displaySearchJokes(searchTerm, shortJokes)}
                  </div>
                )}

                {!searchLoading && mediumJokes && mediumJokes.length > 0 && (
                  <div className="mediumJokes">
                    <b> Medium Jokes - less than 20 words ({mediumJokes.length})</b>
                    {this.displaySearchJokes(searchTerm, mediumJokes)}
                  </div>
                )}

                {!searchLoading && longJokes && longJokes.length > 0 && (
                  <div className="longJokes">
                    <b> Long Jokes - more than 20 words ({longJokes.length})</b>
                    {this.displaySearchJokes(searchTerm, longJokes)}
                  </div>
                )}
              </div>
              <div className="col-md-2"></div>
            </div>
          </div>
        </div>
    );
  }
}
