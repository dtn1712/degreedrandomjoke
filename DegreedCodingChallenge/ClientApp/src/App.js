import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { RandomJoke } from './components/RandomJoke';
import { SearchJoke } from './components/SearchJoke';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={RandomJoke} />
        <Route exact path='/search' component={SearchJoke} />
      </Layout>
    );
  }
}
